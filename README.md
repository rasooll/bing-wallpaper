# Change desktop wallpaper to bing.com

<strong>dependency:</strong>
<code>Wget, Curl</code><br />

<strong>before run:</strong><br>
<code>$ mkdir $HOME/Pictures/Wallpapers/bing/ir</code><br>
<code>$ mkdir $HOME/Pictures/Wallpapers/bing/us</code><br>
<code>$ mkdir $HOME/Pictures/Wallpapers/bing/gb</code><br>

<strong>how to run:</strong><br>
<code>$ ./bing.sh ir</code><br>
<code>$ ./bing.sh us</code><br>
<code>$ ./bing.sh gb</code><br>